import axios from 'axios'
import { IDevice } from '../../../shared/types'

const url = 'https://fonoapi.freshpixl.com/v1/getdevice'
// Token should be provided from env variable, but for those app will be enough :)
const token = 'ecd3452d846427fedae78901fd2cfe25e9e991736be60296'

export async function getDeviceInfo(device: IDevice) {
  const response = await axios.post(url, {
    token,
    device: `${device.brand} ${device.model}`
  })
  if (!response.data.length) return null
  return response.data[0]
}
