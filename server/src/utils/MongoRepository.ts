import * as MongoClient from 'mongodb'

let dbClient
let dbInstance

export async function connectToDb(): Promise<MongoClient.Db> {
  const host = process.env.MONGO_HOST || 'localhost:27017'
  const dbURI = `mongodb://${host}`
  dbClient = await MongoClient.connect(dbURI, { useNewUrlParser: true }).catch(e => {
    throw new Error(e)
  })
  dbInstance = dbClient.db('starter')
  return dbInstance
}

export class MongoRepository {
  db: MongoClient.Db
  collection: MongoClient.Collection

  constructor({ collectionName }) {
    this.db = dbInstance
    this.collection = this.initializeCollection(collectionName)
  }

  initializeCollection(collectionName) {
    return this.db.collection(collectionName)
  }

  getCollection() {
    return this.collection
  }
}
