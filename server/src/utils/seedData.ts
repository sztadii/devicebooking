import { connectToDb } from './MongoRepository'
import { DeviceRepository } from '../graphql/device/DeviceRepository'
import { devices } from '../../../shared/mocks'

async function seedData() {
  try {
    const db = await connectToDb()
    await db.dropDatabase()
    const deviceRepository = new DeviceRepository()
    await Promise.all(devices.map(async device => deviceRepository.createDevice(device)))

    console.log('Success during seed data')
    process.exit(0)
  } catch (e) {
    console.log('Error during seed data')
    process.exit(1)
  }
}

// Ignore promise, we already catch exceptions
seedData()
