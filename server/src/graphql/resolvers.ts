export const resolvers = {
  Query: {
    devices: (parent, args, { deviceRepository }) => deviceRepository.getDevices()
  },
  Mutation: {
    updateDevice: (parent, args, { deviceRepository }) => deviceRepository.updateDevice(args)
  },
  Device: {
    info: (device, args, { getDeviceInfo }) => getDeviceInfo(device)
  }
}
