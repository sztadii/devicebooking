const MongoClient = require('mongodb')
const { MongoRepository } = require('../../utils/MongoRepository')
import { IDevice } from '../../../../shared/types'

export class DeviceRepository extends MongoRepository {
  constructor() {
    super({ collectionName: 'devices' })
  }

  getDevices() {
    return this.getCollection()
      .find()
      .toArray()
  }

  createDevice(obj: IDevice) {
    const device = {
      ...obj,
      _id: new MongoClient.ObjectId().toString()
    }
    return this.getCollection().insertOne(device)
  }

  async updateDevice({ input }) {
    await this.getCollection().findOneAndUpdate(
      {
        _id: input._id
      },
      {
        $set: {
          ...input
        }
      }
    )
    return input
  }
}
