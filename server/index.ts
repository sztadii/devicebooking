import { ApolloServer } from 'apollo-server'
import { importSchema } from 'graphql-import'
import { makeExecutableSchema } from 'graphql-tools'
import { connectToDb } from './src/utils/MongoRepository'
import { DeviceRepository } from './src/graphql/device/DeviceRepository'
import { getDeviceInfo } from './src/services/FonoService'
import { resolvers } from './src/graphql/resolvers'

const schema = makeExecutableSchema({
  typeDefs: importSchema(`${__dirname}/src/graphql/schema.graphql`),
  resolvers
})

async function runServer() {
  try {
    await connectToDb()
    const services = {
      getDeviceInfo
    }
    const repositories = {
      deviceRepository: new DeviceRepository()
    }
    const server = new ApolloServer({
      schema,
      context: () => ({
        ...repositories,
        ...services
      })
    })
    const { url } = await server.listen()
    console.log('Server is running: ' + url)
  } catch (e) {
    console.log('Server is not running')
    console.log(e)
  }
}

// Ignore promise, we already catch exceptions
runServer()
