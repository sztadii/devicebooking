# test_phones

> Device booking application

### DEVELOPMENT SETUP

- Install NodeJS
- Install Docker
- Install requirement dependencies via `npm ci`
- Run app via `npm start`

### TESTING SETUP

- Install NodeJS
- Install Docker
- Install requirement dependencies via `npm ci`
- Run unit tests via `npm test`
- Run e2e tests via `npm run e2e`
