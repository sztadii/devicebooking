import React from 'react'
import { Wrapper } from './Truncate.style'

interface Props {
  text: string
}

export default function Truncate({ text }: Props) {
  return <Wrapper>{text}</Wrapper>
}
