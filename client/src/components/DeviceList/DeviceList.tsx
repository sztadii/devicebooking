import React, { Component } from 'react'
import { IDevice } from '../../../../shared/types'
import DeviceItem from '../DeviceItem/DeviceItem'
import Form from '../Form/Form'
import { Title } from './DeviceList.style'

interface Props {
  devices: Array<IDevice>
}

interface State {
  selectedDevice?: IDevice
}

const initState = {
  selectedDevice: undefined
}

export default class DeviceList extends Component<Props, State> {
  state = {
    ...initState
  }

  handleEdit = (selectedDevice: IDevice) => {
    this.setState({
      selectedDevice
    })
  }

  handleClose = () => {
    this.setState({
      ...initState
    })
  }

  render() {
    const { selectedDevice } = this.state
    const { devices } = this.props
    return (
      <div>
        <Title>Devices</Title>

        {selectedDevice && <Form device={selectedDevice} onClose={this.handleClose} />}

        {devices.map(device => (
          <DeviceItem key={device._id} device={device} onEdit={this.handleEdit} />
        ))}
      </div>
    )
  }
}
