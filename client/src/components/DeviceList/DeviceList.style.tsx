import styled from 'styled-components'

export const Title = styled.h1`
  font-size: 40px;
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: 20px;
`
