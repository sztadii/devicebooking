import styled from 'styled-components'

export const DeviceWrapper = styled.div`
  padding: 20px;
  box-shadow: 0 4px 10px 4px #c3c3c3;
  border: 1px solid #c3c3c3;
  border-radius: 5px;
  margin-bottom: 20px;
  transition: 0.3s;

  &:hover {
    box-shadow: 0 4px 15px 8px #c3c3c3;
  }
`

export const Label = styled.div`
  background: #908b8a;
  color: white;
  padding: 10px 20px;
  font-weight: 400;
  margin-right: 20px;
  max-width: 300px;
  width: 100%;
  border-radius: 20px;
  cursor: pointer;
  transition: 0.3s;

  &:hover {
    transform: translateX(10px);
  }

  &.is-active {
    background: #622332;
  }
`

export const Toggler = styled.div`
  display: none;

  &.is-active {
    margin-top: 20px;
    display: block;
  }
`
