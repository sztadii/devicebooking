import React, { Component } from 'react'
import { IDevice } from '../../../../shared/types'
import PairItem from '../PairItem/PairItem'
import { DeviceWrapper, Label, Toggler } from './DeviceItem.style'

interface Props {
  device: IDevice
  onEdit(device: IDevice): void
}

interface State {
  isToggled: boolean
}

export default class DeviceItem extends Component<Props, State> {
  state = {
    isToggled: false
  }

  handleEdit = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation()
    const { device, onEdit } = this.props
    onEdit(device)
  }

  handleToggle = () => {
    this.setState({
      isToggled: !this.state.isToggled
    })
  }

  render() {
    const { isToggled } = this.state
    const { device } = this.props

    const availabilityClassName = device.currentOwner ? 'is-active' : ''
    const toggleClassName = isToggled ? 'is-active' : ''

    return (
      <DeviceWrapper className={availabilityClassName} onClick={this.handleToggle}>
        <Label
          data-testid="device-item"
          className={availabilityClassName}
          onClick={this.handleEdit}
        >
          {device.customName}
        </Label>

        <Toggler className={toggleClassName}>
          <PairItem label="Custom name" value={device.customName} />
          <PairItem label="Brand" value={device.brand} />
          <PairItem label="Model" value={device.model} />
          <PairItem label="Current owner" value={device.currentOwner} />
          <PairItem label="Book date" value={device.bookDate} />

          {device.info && (
            <div>
              <PairItem label="Technology" value={device.info.technology} />
              <PairItem label="2g bands" value={device.info._2g_bands} />
              <PairItem label="3g bands" value={device.info._3g_bands} />
              <PairItem label="4g bands" value={device.info._4g_bands} />
            </div>
          )}
        </Toggler>
      </DeviceWrapper>
    )
  }
}
