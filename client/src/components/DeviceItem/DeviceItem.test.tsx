import React from 'react'
import { render, cleanup } from 'react-testing-library'
import { devices } from '../../../../shared/mocks'
import DeviceItem from './DeviceItem'

beforeEach(cleanup)

test('DeviceItem display render without error', () => {
  const { getByText } = render(<DeviceItem device={devices[0]} onEdit={() => {}} />)

  getByText('Samsung Galaxy S9')
})

test('DeviceItem clicked in the label trigger onEdit callback', () => {
  const onEdit = jest.fn()

  const expectedArg = {
    _id: 'id-1',
    bookDate: null,
    brand: 'Samsung',
    currentOwner: null,
    customName: 'Samsung Galaxy S9',
    info: {
      _2g_bands: 'Some _2g_bands',
      _3g_bands: 'Some _3g_bands',
      _4g_bands: 'Some string',
      technology: 'Some technology'
    },
    model: 'Galaxy S9'
  }

  const { getByText } = render(<DeviceItem device={devices[0]} onEdit={onEdit} />)

  getByText('Samsung Galaxy S9').click()
  expect(onEdit).toBeCalledTimes(1)
  expect(onEdit).toBeCalledWith(expectedArg)
})
