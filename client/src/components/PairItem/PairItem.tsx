import React from 'react'
import Truncate from '../Truncate/Truncate'
import { Wrapper, Label } from './PairItem.style'

interface Props {
  label: string
  value: any
}

export default function PairItem({ label, value }: Props) {
  return (
    <Wrapper>
      <Label>{label}</Label>
      <Truncate text={value || '-'} />
    </Wrapper>
  )
}
