import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 0;
  border-bottom: 1px solid #c3c3c3;

  @media (min-width: 600px) {
    align-items: center;
    flex-direction: row;
  }
`

export const Label = styled.div`
  width: 30%;
  text-transform: uppercase;
  font-weight: 400;

  @media (min-width: 600px) {
    margin-right: 20px;
  }
`
