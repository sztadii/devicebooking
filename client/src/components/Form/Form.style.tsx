import styled from 'styled-components'

export const Cover = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  animation-name: coverOpacity;
  animation-duration: 0.3s;
  animation-delay: 0s;

  @keyframes coverOpacity {
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
    }
  }
`

export const Box = styled.div`
  padding: 20px;
  border: 1px solid #c3c3c3;
  border-radius: 5px;
  margin-bottom: 20px;
  width: 400px;
  position: absolute;
  top: 30%;
  left: calc(50% - 200px);
  background: white;
  z-index: 2;
  text-align: center;
  animation-name: boxTransform;
  animation-duration: 0.3s;
  animation-delay: 0s;

  @keyframes boxTransform {
    0% {
      transform: translateY(30%);
    }

    100% {
      transform: translateY(0);
    }
  }
`

export const Title = styled.h1`
  font-size: 20px;
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: 20px;
`

export const Label = styled.div`
  margin-bottom: 10px;
`

export const Input = styled.input`
  background: #c3c3c3;
  padding: 10px;
  border: none;
  border-radius: 5px;
  margin-bottom: 20px;
  width: 100%;
`

export const Button = styled.button`
  cursor: pointer;
  border: 1px solid #c3c3c3;
  border-radius: 20px;
  padding: 5px 10px;
  display: inline-block;
`
