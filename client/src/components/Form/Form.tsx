import React, { Component } from 'react'
import { withApollo, WithApolloClient } from 'react-apollo'
import { IDevice } from '../../../../shared/types'
import { DEVICES_QUERY } from '../../graphql/queries'
import { UPDATE_DEVICE } from '../../graphql/mutations'
import { Title, Box, Input, Label, Button, Cover } from './Form.style'

interface Props {
  device: IDevice
  onClose(): void
}

interface State {
  value: string
}

type FormProps = WithApolloClient<Props>

class Form extends Component<FormProps, State> {
  state = {
    value: ''
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      value: e.target.value
    })
  }

  handleSubmit = () => {
    const { device, onClose } = this.props
    const { value } = this.state
    const input: IDevice = {
      _id: device._id,
      customName: device.customName,
      brand: device.brand,
      model: device.model,
      bookDate: value ? new Date().toUTCString() : null,
      currentOwner: value ? value : null
    }
    this.props.client.mutate({
      mutation: UPDATE_DEVICE,
      variables: {
        input
      },
      update: (store, { data: { updateDevice } }) => {
        const data = store.readQuery({ query: DEVICES_QUERY })
        //@ts-ignore
        const devices: Array<IDevice> = data.devices || []
        const index = devices.findIndex(x => x._id == updateDevice._id)
        devices[index] = { ...devices[index], ...updateDevice }
        store.writeQuery({ query: DEVICES_QUERY, data })
        onClose()
      }
    })
  }

  render() {
    const { value } = this.state
    const {
      device: { currentOwner }
    } = this.props
    const isFree = !currentOwner
    return (
      <Cover>
        <Box>
          <Title>{isFree ? 'Set new owner' : `Make it free from ${currentOwner}`}</Title>

          {isFree && (
            <div>
              <Label>{currentOwner}</Label>
              <Input
                placeholder="New owner"
                data-testid="input"
                type="text"
                value={value}
                onChange={this.handleChange}
              />
            </div>
          )}

          <div onClick={this.handleSubmit}>
            <Button>Confirm it</Button>
          </div>
        </Box>
      </Cover>
    )
  }
}

export default withApollo(Form)
