import styled from 'styled-components'

const Message = styled.div`
  background: #b4afae;
  color: white;
  padding: 20px;
  font-weight: 400;
  border-radius: 30px;
  margin: 20px 0;
  text-align: center;
`

export default Message
