import React, { Component } from 'react'
import { Query } from 'react-apollo'
import { DEVICES_QUERY } from '../../graphql/queries'
import DeviceList from '../DeviceList/DeviceList'
import Container from '../Container/Container'
import Message from '../Message/Message'
import { GlobalStyle } from './App.style'

export default class App extends Component {
  render() {
    return (
      <Container>
        <Query query={DEVICES_QUERY}>
          {({ data, loading, error }) => {
            if (loading) return <Message>Loading...</Message>
            if (error) return <Message>Error :(</Message>
            return <DeviceList {...data} />
          }}
        </Query>
        <GlobalStyle />
      </Container>
    )
  }
}
