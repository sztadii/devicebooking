import React from 'react'
import { render, cleanup, wait } from 'react-testing-library'
import { MockedProvider } from 'react-apollo/test-utils'
import { DEVICES_QUERY } from '../../graphql/queries'
import { devices } from '../../../../shared/mocks'
import App from './App'

const mocks = [
  {
    request: {
      query: DEVICES_QUERY
    },
    result: {
      data: {
        devices
      }
    }
  }
]

const errorMocks = [
  {
    request: {
      query: DEVICES_QUERY
    },
    error: new Error('Something went wrong')
  }
]

function renderComponent(mock: any = mocks) {
  return render(
    <MockedProvider mocks={mock} addTypename={false}>
      <App />
    </MockedProvider>
  )
}

beforeEach(cleanup)

test('App render loader during fetching data', () => {
  const { getByText } = renderComponent()

  getByText('Loading...')
})

test('App render error from server', async () => {
  const { getByText } = renderComponent(errorMocks)

  await wait(() => {
    getByText('Error :(')
  })
})

test('App render devices list without errors', async () => {
  const { getByText, queryAllByTestId } = renderComponent()

  await wait(() => {
    getByText(/Samsung Galaxy S9/)
    getByText(/Some technology/)
    expect(queryAllByTestId('device-item')).toHaveLength(6)
  })
})
