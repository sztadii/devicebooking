import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'

export const GlobalStyle = createGlobalStyle`
  ${reset}
  
  * {
    box-sizing: border-box;
    outline: none;
    -webkit-tap-highlight-color: transparent;
  }
  
  body {
    font-family: 'Signika', sans-serif;
    color: black;
    font-size: 18px;
    font-weight: 300;
  }
  
  a {
    text-decoration: none;
    color: inherit;
  }
  
  input {
    //TO REMOVE IOS DEFAULTS
    appearance: none;
    box-shadow: none !important;
  }
`
