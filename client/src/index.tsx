import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App/App'
import { ApolloProvider } from 'react-apollo'
import client from './graphql/client'

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
)
