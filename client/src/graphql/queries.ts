import gql from 'graphql-tag'

export const DEVICES_QUERY = gql`
  {
    devices {
      _id
      customName
      brand
      model
      bookDate
      currentOwner
      info {
        technology
        _2g_bands
        _3g_bands
        _4g_bands
      }
    }
  }
`
