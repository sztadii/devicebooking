import gql from 'graphql-tag'

export const UPDATE_DEVICE = gql`
  mutation UpdateDevice($input: DeviceInput!) {
    updateDevice(input: $input) {
      _id
      customName
      brand
      model
      bookDate
      currentOwner
    }
  }
`
