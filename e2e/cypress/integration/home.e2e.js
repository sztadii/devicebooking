import home from '../pages/HomePage'

test('When user visit main page he will be able to see device list', () => {
  home.visit()
  home.verifyListItemsCount()
})

test('When user click in available device he will be able to book it', () => {
  home.visit()
  home.clickDevice()
  home.fillInput()
  home.submitForm()
  home.verifyBookDevice()
})

test('When user click in booked device he will able to make it available', () => {
  home.visit()
  home.verifyBookDevice()
  home.clickDevice()
  home.submitForm()
  home.verifyAvailableDevice()
})
