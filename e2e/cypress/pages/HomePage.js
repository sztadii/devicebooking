class HomePage {
  constructor(driver) {
    this.driver = driver
  }

  visit() {
    this.driver.visit('/')
  }

  verifyListItemsCount() {
    this.driver.getAllByTestId('device-item').should('have.length', 6)
  }

  clickDevice() {
    this.driver.getByTestId('device-item').click()
  }

  fillInput() {
    this.driver.getAllByTestId('input').type('New owner')
  }

  submitForm() {
    this.driver.getByText('Confirm it').click()
  }

  verifyBookDevice() {
    this.driver.getByText('New owner')
  }

  verifyAvailableDevice() {
    this.driver.contains('New owner').should('not.exist')
  }
}

export default new HomePage(cy)
