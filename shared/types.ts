interface IInfo {
  technology: string
  _2g_bands: string
  _3g_bands: string
  _4g_bands: string
}

export interface IDevice {
  _id: string
  customName: string
  brand: string
  model: string
  bookDate: string | null
  currentOwner: string | null
  info?: IInfo | null
}
