import { IDevice } from './types'

export const devices: Array<IDevice> = [
  {
    _id: 'id-1',
    customName: 'Samsung Galaxy S9',
    brand: 'Samsung',
    model: 'Galaxy S9',
    bookDate: null,
    currentOwner: null,
    info: {
      technology: 'Some technology',
      _2g_bands: 'Some _2g_bands',
      _3g_bands: 'Some _3g_bands',
      _4g_bands: 'Some string'
    }
  },
  {
    _id: 'id-2',
    customName: 'Samsung Galaxy S8',
    brand: 'Samsung',
    model: 'Galaxy S8',
    bookDate: new Date().toUTCString(),
    currentOwner: 'Kamdi',
    info: null
  },
  {
    _id: 'id-3',
    customName: 'Old Galaxy S7',
    brand: 'Samsung',
    model: 'Galaxy S7',
    bookDate: new Date().toUTCString(),
    currentOwner: 'Mano',
    info: null
  },
  {
    _id: 'id-4',
    customName: 'My favourite nexus',
    brand: 'LG',
    model: 'Nexus 6',
    bookDate: new Date().toUTCString(),
    currentOwner: 'Kamdi',
    info: null
  },
  {
    _id: 'id-5',
    customName: "Selvio's old iphone",
    brand: 'Apple',
    model: 'Iphone X',
    bookDate: null,
    currentOwner: null,
    info: null
  },
  {
    _id: 'id-6',
    customName: "Pablo's old iphone",
    brand: 'Apple',
    model: 'Iphone 8',
    bookDate: null,
    currentOwner: null,
    info: null
  }
]
